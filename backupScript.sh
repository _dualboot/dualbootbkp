#!/bin/bash

backup_partitions=(
DBackup0
DBackup1
DBackup2
)

additional_sources=(
$HOME/.kiss
$HOME/.ssh
$HOME/.brave-home
)

rsync_options=-rltvX

for partition_name in ${backup_partitions[@]}; do
	
	destination=/run/media/$USER/$partition_name/$USER
	
	if [[ -d $destination ]]; then
		
		if [[ $1 = --add ]]; then
			rsync $rsync_options --exclude=.* $destination
			rsync $rsync_options $HOME/* $destination
			for item in ${additional_sources[@]}; do
				rsync $rsync_options $item $destination
			done
		
		elif [[ $1 = --sync ]]; then
			rsync $rsync_options --exclude=.* --delete-delay $HOME/ $destination
			rsync $rsync_options --delete-delay $HOME/* $destination
			for item in ${additional_sources[@]}; do
				rsync $rsync_options --delete-delay $item $destination
			done
		
		elif [[ $1 = --restore ]]; then
			rsync $rsync_options $destination/ $HOME
		else
			echo "Opção não reconhecida."
			exit 1
		fi
		
		if (mount | grep $partition_name | grep -q btrfs) then
			notify-send --icon=emblem-synchronizing "Backup complete." "Enter password to create read-only snapshot."
			sudo btrfs subvolume snapshot -r $destination "$destination - $(date +%Y-%m-%d\ %T)"
		else
			notify-send --icon=emblem-synchronizing "Backup complete." "You may close the terminal."
			echo "$partition_name não é BTRFS. Snapshot não será criado."
		fi
	
	fi
done

# rsync a/ /b # copia os conteúdos de dentro de a para dentro de b (copia a para / com o nome b)
# rsync a /b # copia a para dentro de b, como /b/a
# rsync a/* /b equivale a rsync a /b
