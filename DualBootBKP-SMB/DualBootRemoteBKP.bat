set backupvolume=X
set remoteuser=dualboot
set passwd=abcd
set server=192.168.1.234

net use %backupvolume%: /d
net use %backupvolume%: \\%server%\%remoteuser% /user:%remoteuser% %passwd%
robocopy %userprofile% %backupvolume%:\DualBootBKP\%username% /mir /r:3 /w:0 /xa:H /xd %userprofile%\AppData "%userprofile%\Application Data"
net use %backupvolume%: /d
